class Config:
	def __init__(self, path):
		self.player = ""
		self.opts = ""
		self.config = open(path)
		self.setPlayer()
		self.setOpts()
		self.config.close()

	def setPlayer(self):
		for line in self.config:
			if line.startswith("PLAYER"):
				self.player = line.split()[-1]

	def setOpts(self):
		for line in self.config:
			if line.startswith("OPTS"):
				self.opts = line.split()[-1]

	def getPlayer(self):
		return self.player

	def getOpts(self):
		return self.opts
