#!/usr/bin/env python
# version 0.2
# most recent changelog: 
# add much better logic for playing something based on better matching logic
# copyright 2013 hlmtre

import os, sys, pretty # pretty is for coloring
from subprocess import call
from config import Config

from video import Video
from imdb import IMDB

config_location = os.getenv("HOME")
video_dict = {}
includes = set() 
excludes = set()
flag = 0
video_counter = 0
player = ""
opts = ""
c = Config(config_location + "/.watchrc")

imdb = IMDB()

def segregate(line):
	global flag
	global excludes
	global includes
	if flag == 1:
		excludes.add(line)
	else:
		includes.add(line)

# write out a filelisting of watched directories
# utility function for use internal only
def writeList(l):
	out = open(config_location + "/.watchlist", "w+")
	for entry, value in l.iteritems():
		out.write(entry + " \\ " + value.getPath() + "\n")

# print only filenames in flatfile listing
def printList():
	listing = open(config_location + "/.watchlist")
	for line in sorted(listing):
		print line.split("\\")[0]

def readList():
	listing = open(config_location + "/.watchlist")
	for line in listing:
		v = Video(line.split("\\")[1], line.split("\\")[0])
		video_dict[line.split("\\")[0]] = v

# read config for includes and excludes, pass each directory in includes to parse
# set player and options
def readConfig():
	global flag
	global player
	f = open(config_location + "/.watchrc")
	for line in f:
		line = line.rstrip()
		if line.startswith("include:"):
			flag = 0
			continue
		if line.startswith("exclude:"):
			flag = 1
			continue
	#	if line.startswith("PLAYER"):
	#		player = line.split()[-1] # last entry on the line
	#		continue
	#	if line.startswith("OPTS"):
	#		options = line.split()[-1]
	#		continue
		segregate(line)
		for entry in includes:
			parse(entry)

# iterate through file listing in directory
# if file is video, add name to dict as pointer to Video object
def parse(directory):
	global video_counter
	global includes
	global excludes
	for dirname, dirnames, filenames in os.walk(directory,topdown=True):
		for n in dirnames:
			if dirname+"/"+n in excludes: # properly honor excludes again
				dirnames.remove(n)
		for filename in filenames:
			if filename.endswith(".avi") or filename.endswith(".mp4") or filename.endswith(".mkv") or filename.endswith(".rmvb") or filename.endswith(".flv"):
				video_counter += 1
				print str(video_counter)+" added... \r",
				v = Video(dirname, filename)
				video_dict[filename] = v


def listMatches(sourcelist):
	matchlist = []
	terms = " ".join(sourcelist)
	for name, v in video_dict.iteritems():
		if v.returnMatch(sourcelist) == 1: # return only things which match 100% of input strings
			matchlist.append(v)	
	
	return matchlist


# search, and if we find a good match, execute mplayer <filename>
def search(sourcelist):
	success = False	
	matchOne = False
	matchTwo = False
	matchlist = []
	global player
	global opts
	for name, v in video_dict.iteritems():
		if v.returnMatch(sourcelist) > .5: # arbitrary amount of likelihood for a match
			if v.returnMatch(sourcelist) == 1.0: # determine if we have multiple 100% matches
				if matchOne:
					matchTwo = True
				else: matchOne = True

			matchlist.append(v)	

	if matchOne == True and matchTwo == True: # multiple 100% matches, print all
		pretty.printc("Multiple 100% matches; refine search:", 'bright yellow')
		for item in matchlist:
			if item.getRecentMatch() == 1:
				print item.getName() + " matched " + str(item.getRecentMatch()*100) + "%"
	
	elif matchOne == True and matchTwo == False: # only one 100% match, play it
		for item in matchlist:
			if item.getRecentMatch() == 1:
				success = True
				pretty.printc("Playing " + item.getName() + " ...", 'bright green')
				string = item.getFull()
				print player
				call ([c.getPlayer(), c.getOpts() + string])

	else: # allow user to further specify
		if not success:
			if len(matchlist) == 1:
				pretty.printc("Playing " + matchlist[0].getName() + " ...", 'bright green')
				string = matchlist[0].getFull()
				call ([c.getPlayer(), c.getOpts() + string])
			pretty.printc(str(len(matchlist)) + " items matching: ",'bright red')
			for item in matchlist:
				print item.getName() + " matched " + str(item.getRecentMatch()*100) + "%"

if __name__ == "__main__":
	arglist = []
	if len(sys.argv) > 1:
		if sys.argv[1] == "-q" and len(sys.argv) > 2: # return hits listed
			readList()
			pretty.printc("Printing matches for " + " ".join(sys.argv[2:]),"bright green")
			for ret in listMatches(sys.argv[2:]):  # should be everything after ./watch.py -l
				print ret.getName()

		elif sys.argv[1] == "-l":
			if os.stat(config_location + "/.watchlist").st_size == 0: # check if file empty
				writeList(video_dict) # rebuild if true
				printList()
			else:
				printList() # otherwise just print dat shiznit

# rebuild index
		elif sys.argv[1] == "-g":
			#pretty.printc("Generating... this might take some time on network shares.",'white')
			readConfig()
			pretty.printc("Adding videos...","white")
			writeList(video_dict)
			pretty.printc(str(video_counter) + " added.","white")
			pretty.printc("Done.",'bright green')
		elif sys.argv[1] == "-h":
			print "usage: watch.py [-l] [-g] [-q <query terms>]"
			print "-l "

		# print keywords
		elif sys.argv[1] == "-k":
			if os.stat(config_location + "/.watchlist").st_size == 0: # check if file empty
				writeList(video_dict) # rebuild if true

			readList()
			for entry,v in sorted(video_dict.iteritems()):
				print "\n" + entry + ": ",
				for word in v.getKeywords():
					print word,

		elif sys.argv[1] == "-i":
			r = imdb.getBasics(sys.argv[2:])
			if r:
				for k, v in r.iteritems():
					print k + ": " + str(v)
			else: 
				print " ".join(sys.argv[2:]) + " not found."
		else:
			readList()
			for word in sys.argv[1:]:
				arglist.append(word)
			search(arglist)
