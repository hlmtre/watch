import urllib2
import json
from _404exception import _404Exception

class IMDB:

	def __init__(self):
		self.base_url = "http://imdbapi.org/?"
		self.protocol = "type=json"


# dumb datapuller
	def _returnQuery(self, query):
		response = urllib2.urlopen(query)
		text = response.read()
		json_object = json.loads(text)
		return json_object

# generate url-safe titling <foo>+<bar>
	def _sanitizeName(self, title):
		if type(title) is str:
			words = title.split()
			safed_title = "+".join(words)
			title = safed_title

		if type(title) is list:
			title = "+".join(title)

		return title

# internal only, really only for debugging
	def _prettyPrint(self, json_object):
		return json.dumps(json_object, sort_keys=True, indent=4, separators=(',', ': '))
		
# returns json object of entirety of return from imdbapi.org or None on error 
# really a python list of lists
	def getInfo(self, title):
		title = self._sanitizeName(title)

 		query = self.base_url + "title=" + title + "&" + self.protocol # create properly-formed url http://imdbapi.org/?title=<blah>+<foo>
		json_object = self._returnQuery(query)

		if "code" in json_object and "error" in json_object:
			return None
		return json_object

# only return a select few fields
	def getBasics(self, title):
		r = self.getInfo(title)
		d = dict()
		try:
			d["Title"] = r[0]["title"]
			d["Year"] = r[0]["year"]
			d["Director"] = r[0]["directors"]
		except TypeError:
			return None
			# heeheehee
			# raise _404Exception("My hovercraft is full of eels.")
		except KeyError: # we got something back but they don't have one of the fields
			d["Title"] = r[0]["title"]
			d["Year"] = r[0]["year"]
		return d
