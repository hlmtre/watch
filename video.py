import re
import os # for basename and normpath

class Video:

	def __init__(self, path, name):
		self.keywords = set()
		self.path = path.rstrip().lstrip() # remove all whitespace and newlines
		self.name = name.rstrip().lstrip()
		self.recentmatch = 0.0
		for word in re.split(r'[-\s._\[\]]', self.name): # this splits on hyphens, spaces (\s), periods, and underscores, and brackets
			self.keywords.add(word.lower())
#		this next one helps with TV shows, where they are organized like Firefly/<episode name>.mkv
		for word in re.split(r'[-\s._\[\]]', os.path.basename(os.path.normpath(self.path))): # add parent directory of files to keywords
			self.keywords.add(word.lower())
		#for word in re.findall(r'[- \s . _]|[A-Z][^A-Z]*',self.name):
		#	self.keywords.add(word.lower())

	def getName(self):
		return self.name

	def getKeywords(self):
		return self.keywords

	def getPath(self):
		return self.path

	def getFull(self):
		return self.path + "/" + self.name

	def getRecentMatch(self):
		return self.recentmatch

	def returnMatch(self, searches):
		i = 0.0 + len(searches)
		j = 0.0
		for entry in searches:
			if entry.lower() in self.keywords:
				j+=1
		self.recentmatch = j/i
		return j/i
