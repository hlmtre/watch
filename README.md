watch
=====

#### virgil, formerly watch, is a python script for building a database of video files according to directories specified in a ~/.watchrc file.

## it has sadly been essentially obsoleted by a plex setup for me, so development has stalled.

virgil is currently in a very young state. it works well enough that i use it every day without much of an urge to fix horrifying behavior.
you configure virgil in the ~/.watchrc. open the file with

		PLAYER = <preferred player> 
		OPTS = "<preferred arguments to $player>"

specify directories you want watch to watch in ~/.watchrc. inside that file, express the directories you wish you include like so:

		include:
		dir one
		dir two

and the ones you want to exclude (subdirectories of the included ones) below that:

		exclude: 
		dir one
		dir two

then simply run virgil.py -g (to generate a list, ~/.watchlist, of files and their paths) or watch.py -l (to list aforementioned files). 
if you wanna watch something, use watch.py word word word 
example:

		virgil.py scrubs my first kill 

virgil.py will look through my videos, and pick out the best match via its totally shweet keyword algorithm.
if it finds more than one, if one (and only one) matches all specified keywords, it plays that. if there are multiple 100% matches, it prints all of them. otherwise it spits out all the decent matches. 
ideally i will eventually add the capacity to understand a bit of english. so if you watch an episode of scrubs, that finishes, and you want to watch the next one, you could say watch.py next, and it'd figure it out.
this repository includes a sample .watchrc for placing in your home folder, showing includes, excludes, and comments.
happy watching!